package com.smartResponse;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.app.ListActivity;
import android.os.Bundle;
import android.content.*;
import android.database.Cursor;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class EnableLocations extends ListActivity {
    String l_name;
    String l_audio;
    String l_status;
    String l_lng;
    String l_lat;
    Long l_id;
    String l_radius;
    String l_address;
    boolean test =false;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		EventDB db = new EventDB(this);
		ArrayAdapter<String> locations = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice);
		ListView listView = getListView();
		//The code below will populate the list
	    db.open();
        Cursor cursor = db.getAllLocations();
        while (cursor.moveToNext())
        {
            String location = cursor.getString(1);
            locations.add(location);
        }
        db.close();
        listView.setFocusable(false);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
		setListAdapter(locations);
		
		//The code below will check records that are listed as "Enabled" in the database
		db.open();
        Cursor c = db.getAllLocations();
        int i = 0;
        
        while (c.moveToNext())
        {
        	String status = c.getString(7);
            if (status.equals("Enabled")){
                    listView.setItemChecked(i, true);
            }
            i=i+1;
        }
        db.close();

      
	}
	        @Override
			protected void onListItemClick(ListView l, View v, int position, long id) {
	        	  super.onListItemClick(l, v, position, id);
	        	    EventDB db = new EventDB(this);

	        	  l_name = (String) getListAdapter().getItem(position);
	        	  db.open();
	        	  Cursor c = db.getAllLocations();
	        	  while (c.moveToNext())
	  	        {
	  	            String results = c.getString(1); 

	  				if(results.equals(l_name))
	  				{
			  			l_id = c.getLong(0);
		  				l_address = c.getString(2);
		  				l_lng = c.getString(3);
		  				l_lat = c.getString(4);
		  				l_radius = c.getString(5);
		  				l_audio = c.getString(6);
		  				l_status = c.getString(7);
		  				if (l_status.equals("Enabled")){
		  					test = true;
		  				}
	  				}
	  	        }
	  	        //end lookup
	  	        db.close();
	  	        if (test == true){
	  	  	      updatelocation(l_id, l_name,l_address,l_lng,l_lat,l_radius,l_audio,"Disabled");
	        	  Toast.makeText(this,l_name + " Disabled", Toast.LENGTH_SHORT).show();
	        	  test = false;
	  	        }
	  	        else{
	  	  	      updatelocation(l_id, l_name,l_address,l_lng,l_lat,l_radius,l_audio,"Enabled");
	        	  Toast.makeText(this,l_name + " Enabled", Toast.LENGTH_SHORT).show();
	        	  test = false;
	  	        }
	        	}

	
	    public void updatelocation(long id, String name, String address, String longitude, 
				String latitude, String radius, String audio, String status)
	    {
	        EventDB db = new EventDB(this);
	        db.open();        
	        db.updateLocation(
	        		id,
	        		name,
	        		address,
	        		longitude,
	        		latitude,
	        		radius,
	        		audio,
	        		status);        
	        db.close();
	    	
	    } 

//Menu Layout
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			super.onCreateOptionsMenu(menu);
			menu.add(0,1,0,"Home").setIcon(R.drawable.home);
	        menu.add(0,3,0,"Cancel").setIcon(R.drawable.cancel);
		    return true;
		}
	    @Override
	     public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getItemId()) {
	        case 1:
			  Intent home_intent_menu = new Intent(EnableLocations.this, MainActivity.class);
			  startActivity(home_intent_menu);
			  return true;
	        case 2:
	    	 	finish();
	            return true;
	          }
			return false;
	    }
}