package com.smartResponse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class EventDB 
{
	//Declaration of Location Table Variables
    public static final String LOCATION_ID = "l_id";
    public static final String LOCATION_NAME = "l_name";
    public static final String LOCATION_ADDRESS = "l_address";
    public static final String LOCATION_LONG = "l_long";
    public static final String LOCATION_LAT = "l_lat";    
    public static final String LOCATION_RADIUS = "l_rad";
    public static final String LOCATION_AUDIO = "l_audio";
    public static final String LOCATION_STATUS = "l_status";
    private static final String LOCATION_TABLE = "GPS_locations";
    
	//Declaration of Message Event Variables
    public static final String MESSAGE_ID = "m_id";
    public static final String MESSAGE_NAME = "m_name";
    public static final String MESSAGE_TEXT = "m_message";
    public static final String MESSAGE_FREQUENCY = "m_frequency";  
    public static final String MESSAGE_DELAY_TIME = "d_time";
    public static final String MESSAGE_LOCATION_NAME = "m_l_name"; 
    private static final String MESSAGE_TABLE = "Message_events";
    
    
    private static final String TAG = "EventDB";
    private static final String DATABASE_NAME = "EventDB";
    private static final int DATABASE_VERSION = 20;

    private static final String DATABASE_CREATE_LOCATION_TABLE =
        "create table "  + LOCATION_TABLE + " (" + LOCATION_ID + " integer primary key autoincrement, "
        + LOCATION_NAME + " text not null, " + LOCATION_ADDRESS + " text not null, " + LOCATION_LONG + " text not null, " 
        + LOCATION_LAT + " text not null, " + LOCATION_RADIUS + " text not null, "
        + LOCATION_AUDIO + " text not null, " + LOCATION_STATUS + " text not null);";
    private static final String DATABASE_CREATE_MESSAGE_TABLE =
            "create table "  + MESSAGE_TABLE + " (" + MESSAGE_ID + " integer primary key autoincrement, "
            + MESSAGE_NAME + " text not null, " + MESSAGE_TEXT + " text not null, " + MESSAGE_FREQUENCY + " text not null, " 
            + MESSAGE_DELAY_TIME + " text not null, " + MESSAGE_LOCATION_NAME + " text not null);";
        
    private final Context context; 
    
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public EventDB(Context ctx) 
    {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
        
    }
        
    private static class DatabaseHelper extends SQLiteOpenHelper 
    {
        DatabaseHelper(Context context) 
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) 
        {
            db.execSQL(DATABASE_CREATE_LOCATION_TABLE);
            db.execSQL(DATABASE_CREATE_MESSAGE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, 
        int newVersion) 
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion 
                    + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + LOCATION_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + MESSAGE_TABLE);
            onCreate(db);
        }
    }    
    
    //---opens the database---
    public EventDB open() throws SQLException 
    {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---closes the database---    
    public void close() 
    {
        DBHelper.close();
    }
    
    public Cursor query(String query) 
    {
        db.execSQL(query);
		return null;
    }
    
    //---insert a location into the database---
    public long insertLocation(String l_name, String l_address, String l_long, String l_lat, 
    		String l_radius, String l_audio, String l_status) 
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put(LOCATION_NAME, l_name);
        initialValues.put(LOCATION_ADDRESS, l_address);
        initialValues.put(LOCATION_LONG, l_long);
        initialValues.put(LOCATION_LAT, l_lat);
        initialValues.put(LOCATION_RADIUS, l_radius);
        initialValues.put(LOCATION_AUDIO, l_audio);
        initialValues.put(LOCATION_STATUS, l_status);
        return db.insert(LOCATION_TABLE, null, initialValues);
    }
    //---insert a message event into the database---
    public long insertMessage(String m_name, String m_text, String m_freq, String d_time, String l_name) 
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put(MESSAGE_NAME, m_name);
        initialValues.put(MESSAGE_TEXT, m_text);
        initialValues.put(MESSAGE_FREQUENCY, m_freq);
        initialValues.put(MESSAGE_DELAY_TIME, d_time);
        initialValues.put(MESSAGE_LOCATION_NAME, l_name);
        return db.insert(MESSAGE_TABLE, null, initialValues);
    }
    //---deletes a location---
    public boolean deleteLocation(long l_id) 
    {

        return db.delete(LOCATION_TABLE, LOCATION_ID + 
        		"=" + l_id, null) > 0;
    }
    //---deletes a message---
    public boolean deleteMessage(long m_id) 
    {

        return db.delete(MESSAGE_TABLE, MESSAGE_ID + 
        		"=" + m_id, null) > 0;
    }
    //---deletes all records of the Location Table
    public void deleteAllLocations() {
        this.db.delete(LOCATION_TABLE, null, null);
    }
    //---deletes all records of the Message Table
    public void deleteAllMessages() {
        this.db.delete(MESSAGE_TABLE, null, null);
    }
    //---retrieves all the locations---
    public Cursor getAllLocations() 
    {
        return db.query(LOCATION_TABLE, new String[] {
        		LOCATION_ID, 
        		LOCATION_NAME,
        		LOCATION_ADDRESS,
        		LOCATION_LONG,
                LOCATION_LAT,
                LOCATION_RADIUS,
                LOCATION_AUDIO,
                LOCATION_STATUS}, 
                null, 
                null, 
                null, 
                null, 
                null,
                null);
    }
    //---retrieves all the messages---
    public Cursor getAllMessages() 
    {
        return db.query(MESSAGE_TABLE, new String[] {
        		MESSAGE_ID, 
        		MESSAGE_NAME,
        		MESSAGE_TEXT,
        		MESSAGE_FREQUENCY,
        		MESSAGE_DELAY_TIME,
        		MESSAGE_LOCATION_NAME}, 
                null, 
                null, 
                null, 
                null, 
                null,
                null);
    }

    //---retrieves a particular location---
    public Cursor getLocation(long l_id) throws SQLException 
    {
        Cursor mCursor =
                db.query(true, LOCATION_TABLE, new String[] {
                		LOCATION_ID, 
                		LOCATION_NAME,
                		LOCATION_ADDRESS,
                		LOCATION_LONG,
                        LOCATION_LAT,
                        LOCATION_RADIUS,
                        LOCATION_AUDIO,
                        LOCATION_STATUS}, 
                		LOCATION_ID + "=" + l_id, 
                		null,
                		null, 
                		null, 
                		null, 
                		null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }
    //---retrieves a particular message---
    public Cursor getMessage(long m_id) throws SQLException 
    {
        Cursor mCursor =
                db.query(true, MESSAGE_TABLE, new String[] {
                		MESSAGE_ID, 
                		MESSAGE_NAME,
                		MESSAGE_TEXT,
                		MESSAGE_FREQUENCY,
                		MESSAGE_DELAY_TIME,
                		MESSAGE_LOCATION_NAME}, 
                		MESSAGE_ID + "=" + m_id, 
                		null,
                		null, 
                		null, 
                		null, 
                		null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }
    //---updates a location---
    public boolean updateLocation(long l_id, String l_name, String l_address,
    String l_long, String l_lat, String l_radius, String l_audio, String l_status) 
    {
        ContentValues args = new ContentValues();
        args.put(LOCATION_NAME, l_name);
        args.put(LOCATION_ADDRESS, l_address);
        args.put(LOCATION_LONG, l_long);
        args.put(LOCATION_LAT, l_lat);
        args.put(LOCATION_RADIUS, l_radius);
        args.put(LOCATION_AUDIO, l_audio);
        args.put(LOCATION_STATUS, l_status);
        return db.update(LOCATION_TABLE, args, 
                         LOCATION_ID + "=" + l_id, null) > 0;
    }
    //---updates a message---
    public boolean updateMessage(long m_id, String m_name, String m_text, 
    		String m_freq, String d_time, String l_name) 
    {
        ContentValues args = new ContentValues();
        args.put(MESSAGE_NAME, m_name);
        args.put(MESSAGE_TEXT, m_text);
        args.put(MESSAGE_FREQUENCY, m_freq);
        args.put(MESSAGE_DELAY_TIME, d_time);
        args.put(MESSAGE_LOCATION_NAME, l_name);
        return db.update(MESSAGE_TABLE, args, 
                         MESSAGE_ID + "=" + m_id, null) > 0;
    }
}
