package com.smartResponse;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;

@SuppressWarnings("deprecation")
public class SmartResponseActivity extends TabActivity {
    /** Called when the activity is first created. */
	static int tabNum = 0;
	public static void setCurrentTab(int tabNumber){
		tabNum = tabNumber;
	 }
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.main);

	    Resources res = getResources(); 
	    final TabHost tabHost = getTabHost();  
	    tabHost.addTab(tabHost.newTabSpec("GPS Settings").setIndicator("GPS",res.getDrawable(R.drawable.gps_icon2)).setContent(new Intent(this, LocationLists.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
	    tabHost.addTab(tabHost.newTabSpec("Message Settings").setIndicator("Messages",res.getDrawable(R.drawable.sms)).setContent(new Intent(this, MessageLists.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
        tabHost.addTab(tabHost.newTabSpec("View all Locations").setIndicator("Location Map",res.getDrawable(R.drawable.map_icon)).setContent(new Intent(this, MapViewAllLocations.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
    //    tabHost.addTab(tabHost.newTabSpec("Power & Efficiency").setIndicator("Power & Efficiency",res.getDrawable(R.drawable.battery)).setContent(new Intent(this, PowerSettings.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
        tabHost.setCurrentTab(tabNum);
	    tabHost.setOnTabChangedListener(new OnTabChangeListener(){
	    	public void onTabChanged(String tabId) {
	    		setTabColor(tabHost);  	
	    	}
	    	});
	    setTabColor(tabHost);
	}

	public static void setTabColor(TabHost tabhost) {
	    for(int i=0;i<tabhost.getTabWidget().getChildCount();i++)
	    {
	        tabhost.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#000050")); //unselected
	    }
	    tabhost.getTabWidget().getChildAt(tabhost.getCurrentTab()).setBackgroundColor(Color.parseColor("#00FFFF")); // selected
	}
	
}