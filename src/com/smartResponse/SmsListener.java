package com.smartResponse;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SmsListener extends BroadcastReceiver{

		
	    @Override
	    public void onReceive(Context context, Intent intent) 
	    {

            Intent serviceIntent = new Intent (context, MessageReply.class);
            serviceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
	    	String msg_from = "Null";
	        //---get the SMS message passed in---
	        Bundle bundle = intent.getExtras();        
	        SmsMessage[] msgs = null;
	        @SuppressWarnings("unused")
			String str = "";            
	        if (bundle != null)
	        {
	        	context.stopService(serviceIntent);
	            //---retrieve the SMS message received---
	            Object[] pdus = (Object[]) bundle.get("pdus");
	            msgs = new SmsMessage[pdus.length];            
	            for (int i=0; i<msgs.length; i++){
	                msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);     
	                msg_from= msgs[i].getOriginatingAddress();   
	                str += "SMS from " + msgs[i].getOriginatingAddress();                     
	                str += " :";
	                str += msgs[i].getMessageBody().toString();
	                str += "\n";        
	                
	            }
	            //---display the new SMS message---
	           // Log.d("msg_from_smslistner", msg_from);
	            MessageReply.setMessageFrom(msg_from, "Null");
                context.startService(serviceIntent);
	           // Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
	        }                         
	    }
	}