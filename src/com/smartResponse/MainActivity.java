package com.smartResponse;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity {
	static ToggleButton toggle;
	EventDB db = new EventDB(this);
	boolean registerStatus = false;
	public static String Status(){
		String status = toggle.getText().toString();
		return status;
	 }
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.mainlayout);
        
	    	ToggleSmartResponse();
	    	EnableLocations();
	        ToggleGPS();
	}

	public void GPSAlert() {

		    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder.setMessage("GPS needs to be enabled in order to ensure accurate location updates. Would you like to enable the GPS locations now?")
		           .setCancelable(false)
		           .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		               public void onClick(final DialogInterface dialog, final int id) {
		                   startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		               }
		           })
		           .setNegativeButton("No", new DialogInterface.OnClickListener() {
		               public void onClick(final DialogInterface dialog, final int id) {
		                    dialog.cancel();
		               }
		           });
		    final AlertDialog alert = builder.create();
		    alert.show();;

	}
	
	public void No_Locations_Alert() {

	    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setMessage("There are no locations to Enable, would you like to add new locations and message events?")
	           .setCancelable(false)
	           .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	               public void onClick(final DialogInterface dialog, final int id) {
	             	  SmartResponseActivity.setCurrentTab(0);
	    			  Intent settings_intent_menu = new Intent(MainActivity.this, SmartResponseActivity.class);
	    			  startActivity(settings_intent_menu);
	               }
	           })
	           .setNegativeButton("No", new DialogInterface.OnClickListener() {
	               public void onClick(final DialogInterface dialog, final int id) {
	                    dialog.cancel();
	               }
	           });
	    final AlertDialog alert = builder.create();
	    alert.show();;

}
	
	//toggle On Button
	public void ToggleSmartResponse() {
		 
		toggle = (ToggleButton) findViewById(R.id.SR_Toggle);
 
		toggle.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
				String status = toggle.getText().toString();
				if (status.equals("ON")){
				    final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
				    if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
					GPSAlert();
				    }
		            Intent serviceIntent = new Intent (MainActivity.this, AudioService.class);
	                startService(serviceIntent);

				}
				if (status.equals("OFF")){
		            Intent serviceIntent = new Intent (MainActivity.this, AudioService.class);
	                stopService(serviceIntent);
	                MessageReply.setMessageFrom("--NULL--", "--NULL--");
				}
				Toast.makeText(getBaseContext(),
						"You application is now "+ status,
						Toast.LENGTH_LONG).show();
			}
		});
	}	

	//toggle GPS Button
	public void ToggleGPS() {
		 
		ImageButton imageButton = (ImageButton) findViewById(R.id.gps_settings2);
 
		imageButton.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
 
				Intent myIntent = new Intent( Settings.ACTION_SECURITY_SETTINGS );
				        startActivity(myIntent);
			}
		});
	}
	//Enable or Disable selected GPS locations
	public void EnableLocations() {
		 
		ImageButton imageButton = (ImageButton) findViewById(R.id.view_icon_id);
 
		imageButton.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
				//The code below will determine if there are any locations
			    
			    
			    boolean empty = true;
			    db.open();
		        Cursor cursor = db.getAllLocations();
		        while (cursor.moveToNext()) {
					   Intent i = new Intent(MainActivity.this, EnableLocations.class);
					   startActivity(i);
					   empty = false;   
		        }
		        db.close();
				if (empty == true){
					No_Locations_Alert();
				}
			}
		});
	}	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	    	Intent startMain = new Intent(Intent.ACTION_MAIN);
	    	startMain.addCategory(Intent.CATEGORY_HOME);
	    	startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    	startActivity(startMain);
	        return true;
	    }

	    return super.onKeyDown(keyCode, event);
	}
	// Menu Layout
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
        menu.add(0,1,0,"Settings").setIcon(R.drawable.settings2);
        menu.add(0,2,0,"Exit").setIcon(R.drawable.exit);
        menu.add(0,3,0,"Help").setIcon(R.drawable.help);
	    return true;
	}
    @Override
     public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case 1:
        	  SmartResponseActivity.setCurrentTab(0);
			  Intent settings_intent_menu = new Intent(MainActivity.this, SmartResponseActivity.class);
			  startActivity(settings_intent_menu);
			  
			  return true;
        case 2:
        	Intent serviceIntent = new Intent (MainActivity.this, AudioService.class);
            stopService(serviceIntent);
            MessageReply.setMessageFrom("--NULL--", "--NULL--");
        	finish();
            return true;
        case 3:
        	String URL= "http://youtu.be/swprsQhnYHE";
        	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
        	startActivity(intent);
            return true;
        }
		return false;
    }
}
