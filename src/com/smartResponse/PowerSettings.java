package com.smartResponse;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class PowerSettings extends Activity {

	private double distance;
	private long time;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.powersettings);


        on_Save_Button_Click();
        on_Low_Button_Click();
        on_Medium_Button_Click();
        on_High_Button_Click();
}

	public void on_Save_Button_Click() {
		 
		Button Button = (Button) findViewById(R.id.save);
 
		Button.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
				//Stops current services
				if (MainActivity.Status().equals("ON")){
				  Intent serviceIntent = new Intent (PowerSettings.this, AudioService.class);
	                stopService(serviceIntent);
	                MessageReply.setMessageFrom("--NULL--", "--NULL--");
	            //Restarts audio service with changed values
	            AudioService.Power_Consumption(time, distance);    
	            
	            startService(serviceIntent);
				}
				if (MainActivity.Status().equals("OFF")){
		            AudioService.Power_Consumption(time, distance);    
					}

			}

		} );
}
	public void on_Low_Button_Click() {
		 
		Button Button = (Button) findViewById(R.id.low);
 
		Button.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
				distance = 25;
				time = 250;
			}

		} );
}
	public void on_Medium_Button_Click() {
		 
		Button Button = (Button) findViewById(R.id.medium);
 
		Button.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
				distance = 25;
				time = 250;
			}

		} );
}
	public void on_High_Button_Click() {
		 
		Button Button = (Button) findViewById(R.id.high);
 
		Button.setOnClickListener(new OnClickListener() {
 
			public void onClick(View v) {
				distance = 25;
				time = 250;
			}

		} );
}


// Menu Layout
@Override
public boolean onCreateOptionsMenu(Menu menu) {
	super.onCreateOptionsMenu(menu);
    menu.add(0,1,0,"Home").setIcon(R.drawable.home);
    return true;
}
@Override
 public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
    case 1:
		  Intent home_intent_menu = new Intent(PowerSettings.this, MainActivity.class);
		  startActivity(home_intent_menu);
		  
		  return true;
      }
	return false;
}
}