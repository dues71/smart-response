package com.smartResponse;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class ViewMessage extends Activity {
		private static String m_name;
		private static String l_name;
	    //define db as an EventDB
	    EventDB db = new EventDB(this);
		private static boolean afterEdit;
		//This function sets the new message name to be looked up later
		public static void change_name(String name, boolean bool){
			m_name = name;
			afterEdit = bool;
		             }
		//onCreate function
		@Override
		public void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
		    setContentView(R.layout.viewmessagelayout);

		    //This method gets data that is passed into ViewMessage.java 
		    Bundle extras = getIntent().getExtras(); 
		    if(extras !=null){
		    	if (afterEdit != true){
		    		Log.d("extras", "Have been imported");
		    		m_name = extras.getString("message");
		    	}
		    }
    		TextView tv_name = new TextView(this);
    		tv_name=(TextView)findViewById(R.id.tv_m_name); 
    		tv_name.setText(m_name);
		    //Get the rest of the information from the database for the message
		    db.open();
	        Cursor c = db.getAllMessages();
	  		TextView tv_message = new TextView(this);
			tv_message=(TextView)findViewById(R.id.tv_m_message);
    		TextView tv_freq = new TextView(this);
			tv_freq=(TextView)findViewById(R.id.tv_m_freq);
			TextView tv_l_id = new TextView(this);
			tv_l_id=(TextView)findViewById(R.id.tv_l_id);
			
			//finds the record in the database from a message name
	        while (c.moveToNext())
	        {
	            String results = c.getString(1); 

				if(results.equals(m_name))
				{
				tv_message.setText(c.getString(2));
				tv_freq.setText(c.getString(3)+" Seconds");
				tv_l_id.setText(c.getString(5));
				l_name = c.getString(5);
				}
	        }
	        //end lookup
	        
	        db.close();
	        edit_message_button_click();
	        remove_message_button_click();

}
		public void edit_message_button_click() {
			 
			ImageButton imageButton = (ImageButton) findViewById(R.id.edit_icon_id);
			
			imageButton.setOnClickListener(new OnClickListener() {
	 
				public void onClick(View v) {
						Intent edit_intent = new Intent(ViewMessage.this, EditMessage.class);
			   			edit_intent.putExtra("message", m_name);
			   			edit_intent.putExtra("location", l_name);
			   			startActivityForResult(edit_intent,1);
				}
	 
			});
	 
		}
		
		public void remove_message_button_click() {
			
			ImageButton imageButton = (ImageButton) findViewById(R.id.delete_icon_id);
			
			imageButton.setOnClickListener(new OnClickListener() {
	 
				public void onClick(View v) {
	 
	     			  	Intent remove_intent = new Intent(ViewMessage.this, RemoveMessage.class);
	     			  	remove_intent.putExtra("message", m_name);
	       				remove_intent.putExtra("location", l_name);
	       				startActivityForResult(remove_intent,1);
				}
			});
		}
		//The code in the function below checks to see if a new message was added and if so it refreshes the page
	    @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        super.onActivityResult(requestCode, resultCode, data);
	        
	        if(resultCode==5){
		        Intent in = new Intent();
		        setResult(1,in);//Here I am Setting the Requestcode 1, you can put according to your requirement
		        finish();
	        }
	        if(resultCode==1){
	            Intent refresh = new Intent(ViewMessage.this, ViewMessage.class);
	            refresh.putExtra("message", m_name);
	            startActivity(refresh);
	            this.finish();
	        }
	        if(resultCode==RESULT_CANCELED){
	        }
	        
		}
// Menu Layout
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			super.onCreateOptionsMenu(menu);
			menu.add(0,1,0,"Home").setIcon(R.drawable.home);
	        menu.add(0,2,0,"Edit").setIcon(R.drawable.edit_icon);
	        menu.add(0,3,0,"Delete").setIcon(R.drawable.delete_icon);
	        menu.add(0,4,0,"Cancel").setIcon(R.drawable.cancel);
		    return true;
		}
	    @Override
	     public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getItemId()) {
	        case 1:
					Intent home_intent_menu = new Intent(ViewMessage.this, MainActivity.class);
					startActivity(home_intent_menu);
   			  
   			  return true;
	        case 2:
		   			Intent edit_intent_menu = new Intent(ViewMessage.this, EditMessage.class);
		   			edit_intent_menu.putExtra("message", m_name);
		   			edit_intent_menu.putExtra("location", l_name);
		   			startActivityForResult(edit_intent_menu,1);
			  this.finish();
   			  
   			  return true;
	        case 3:
     			  	Intent remove_intent_menu = new Intent(ViewMessage.this, RemoveMessage.class);
     			  	remove_intent_menu.putExtra("message", m_name);
       				remove_intent_menu.putExtra("location", l_name);
       				startActivityForResult(remove_intent_menu,1);
       				this.finish();

	            return true;
	     case 4:
					SmartResponseActivity.setCurrentTab(1);
					Intent message_intent_menu = new Intent(ViewMessage.this, SmartResponseActivity.class);
					startActivity(message_intent_menu);
					finish();
	            return true;
	          }
			return false;
	    }
}