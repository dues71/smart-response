package com.smartResponse;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class RemoveLocation extends Activity {
	EventDB db = new EventDB(this);
		@Override
		public void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
		    setContentView(R.layout.removelocationlayout);
	        yes_click();
	        no_click();
}
		public void yes_click() {
			 
			Button Button = (Button) findViewById(R.id.l_yes);
	 
			Button.setOnClickListener(new OnClickListener() {
	 
				public void onClick(View v) {
					Bundle extras = getIntent().getExtras(); 
					long id = 0;
				    db.open();
				    Cursor c = db.getAllLocations();
			        while (c.moveToNext())
			        {
			            String results = c.getString(1); 

						if(results.equals(extras.getString("location")))
						{
						id = c.getLong(0);
						}
			        }
				    db.query("DELETE FROM GPS_locations WHERE l_id = '" + id +"'");
			        db.close();
					
			        Intent in = new Intent();
			        setResult(5,in);//Here I am Setting the Requestcode 1, you can put according to your requirement
			        finish();			
					}

			} );
	}
		public void no_click() {
			 
			Button Button = (Button) findViewById(R.id.l_no);
	 
			Button.setOnClickListener(new OnClickListener() {
	 
				public void onClick(View v) {
			        Intent in = new Intent();
			        setResult(RESULT_CANCELED,in);//Here I am Setting the Requestcode 1, you can put according to your requirement
			        finish();
			}

			} );
	}
			
		// Menu Layout
				@Override
				public boolean onCreateOptionsMenu(Menu menu) {
					super.onCreateOptionsMenu(menu);
					menu.add(0,1,0,"Home").setIcon(R.drawable.home);
			        menu.add(0,2,0,"Delete").setIcon(R.drawable.delete_icon);
			        menu.add(0,3,0,"Cancel").setIcon(R.drawable.cancel);
				    return true;
				}
			    @Override
			     public boolean onOptionsItemSelected(MenuItem item) {
			        switch (item.getItemId()) {
			        case 1:
						  Intent home_intent_menu = new Intent(RemoveLocation.this, MainActivity.class);
						  startActivity(home_intent_menu);
		   			  
		   			  return true;
			        case 2:
						Bundle extras = getIntent().getExtras(); 
					    db.open();
					    db.query("DELETE FROM GPS_locations WHERE l_name = '" + extras.getString("location")+"'");
				        db.close();
						
				        Intent in = new Intent();
				        setResult(5,in);//Here I am Setting the Requestcode 1, you can put according to your requirement
				        finish();			
						

			            return true;
			     case 3:
				        Intent i = new Intent();
				        setResult(RESULT_CANCELED,i);//Here I am Setting the Requestcode 1, you can put according to your requirement
				        finish();
			          }
					return false;
			    }
		}