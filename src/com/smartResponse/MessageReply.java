package com.smartResponse;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.util.Log;

public class MessageReply extends Service {
	static String msg_from = "Null";
	static String l_name = "Null";
	public static void setMessageFrom(String from, String location){
		if (from != "Null"){
		msg_from = from;
		}
		if (location != "Null"){
		l_name = location;
		}
	             }
	@Override
	public void onCreate(){
		String timeDelay = "Null";
		long m_id = 0;
		String m_name = "Null";
		long curTime = System.currentTimeMillis();
		long delayTime = 0;
		super.onCreate();
		Log.d("MRonCreate()","Service Created");
		String msg = "Null";
		EventDB db = new EventDB(this);
		Log.d("MessageREply l_name", l_name);
		Log.d("MessageREply msg_from", msg_from);
		if (l_name != "Null" && msg_from !="Null"){
		db.open();
        Cursor c2 = db.getAllMessages();
        while (c2.moveToNext())
        {
            String results = c2.getString(5);
            if (l_name.equals(results)){
        		m_id = c2.getLong(0);
        		m_name = c2.getString(1);
            	msg = c2.getString(2);
            	timeDelay = c2.getString(3);
        		if (c2.getString(4).equals("Null")){
        			delayTime = (curTime + (Long.parseLong(timeDelay)*1000));
        		}else{
        			delayTime = Long.parseLong(c2.getString(4));
        		}
            }
        }
        db.close();
        Log.d("timeDelay",timeDelay);
    	Log.d("curTime"," "+ curTime);
    	Log.d("delayTime"," " + delayTime);
    	Log.d("timeDelay",timeDelay);
    	
        if (msg != "Null"){
        	if (delayTime != (curTime + (Long.parseLong(timeDelay)*1000))){
	            if (curTime > delayTime){
	       		 sendSMS(msg_from, msg);
	       		 delayTime = curTime + (Long.parseLong(timeDelay)*1000);
	            }
        	}
        	else{
        		sendSMS(msg_from, msg);
        	}
        }
        updateMessage(m_id, m_name, msg, timeDelay, String.valueOf(delayTime), l_name);
  	  
	}
		}
	@Override
	public void onStart(Intent intent, int startId){
		super.onStart(intent, startId);
		Log.d("MRonStart()","Service Started");
	}

		//---sends an SMS message to another device---
		    private void sendSMS(String phoneNumber, String message)
		    {        
		        Log.v("phoneNumber",phoneNumber);
		        Log.v("Message",message);       
		        PendingIntent si = PendingIntent.getBroadcast(this, 0, new 
		        		Intent("Example"), 0); 

		        SmsManager sms = SmsManager.getDefault();
		        sms.sendTextMessage(phoneNumber, null, message, si, null);     
		        ContentValues values = new ContentValues();
		        // The code below adds the autoreply SMS to the conversation
		        values.put("address", phoneNumber);
		        values.put("body", message);
		        try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        getContentResolver().insert(Uri.parse("content://sms/sent"), values);
		    }
		@Override
		public IBinder onBind(Intent arg0) {
			// TODO Auto-generated method stub
			return null;
		}    
	    public void updateMessage(long id, String name, String message, String freq, String d_time, String l_name)
	    {
	    	EventDB db = new EventDB(this);
	        db.open();        
	        db.updateMessage(
	        		id,
	        		name,
	        		message,
	        		freq,
	        		d_time,
	        		l_name);      
	        db.close();
	    	
	    } 
}
