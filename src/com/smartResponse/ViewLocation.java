package com.smartResponse;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class ViewLocation extends Activity {
		private static String l_name;
		private boolean afterEdit;
		//This function sets the new location name to be looked up later
		public static void change_name(String name){
			l_name = name;
		             }
		//onCreate function
		@Override
		public void onCreate(Bundle savedInstanceState) {
		    super.onCreate(savedInstanceState);
		    setContentView(R.layout.viewlocationlayout);
		    //define db as an EventDB
		    EventDB db = new EventDB(this);
		    //This method gets data that is passed into ViewLocation.java 
		    Bundle extras = getIntent().getExtras(); 
		    if(extras !=null){
		    	if (afterEdit == false){
		    		l_name = extras.getString("location");
		    	}
		    }
    		TextView tv_name = new TextView(this);
    		tv_name=(TextView)findViewById(R.id.tv_l_name); 
    		tv_name.setText(l_name);
		    //Get the rest of the information from the database for the location
		    db.open();
	        Cursor c = db.getAllLocations();
	  		TextView tv_address = new TextView(this);
			tv_address=(TextView)findViewById(R.id.tv_l_address);
    		TextView tv_long = new TextView(this);
			tv_long=(TextView)findViewById(R.id.tv_l_long);
			TextView tv_lat = new TextView(this);
			tv_lat=(TextView)findViewById(R.id.tv_l_lat); 
			TextView tv_radius = new TextView(this);
			tv_radius=(TextView)findViewById(R.id.tv_l_radius); 
			TextView tv_audio = new TextView(this);
			tv_audio =(TextView)findViewById(R.id.tv_l_audio); 
			TextView tv_status = new TextView(this);
			tv_status =(TextView)findViewById(R.id.tv_l_status); 
			
			//finds the record in the database from a location name
	        while (c.moveToNext())
	        {
	            String results = c.getString(1); 

				if(results.equals(l_name))
				{
				tv_address.setText(c.getString(2));
				tv_long.setText(c.getString(3));
				tv_lat.setText(c.getString(4));
				tv_radius.setText(c.getString(5)+" Feet");
				tv_audio.setText(c.getString(6));
				tv_status.setText(c.getString(7));
				}
	        }
	        //end lookup
	        
	        db.close();
	        edit_location_button_click();
	        remove_location_button_click();

}
		public void edit_location_button_click() {
			 
			ImageButton imageButton = (ImageButton) findViewById(R.id.edit_icon_id);
			
			imageButton.setOnClickListener(new OnClickListener() {
	 
				public void onClick(View v) {
		   			  Intent edit_intent = new Intent(ViewLocation.this, EditLocation.class);
		   			  edit_intent.putExtra("location", l_name);
		   			  startActivityForResult(edit_intent,1);
				}
	 
			});
	 
		}
		
		public void remove_location_button_click() {
			
			ImageButton imageButton = (ImageButton) findViewById(R.id.delete_icon_id);
			
			imageButton.setOnClickListener(new OnClickListener() {
	 
				public void onClick(View v) {
	 
	     			  Intent remove_intent = new Intent(ViewLocation.this, RemoveLocation.class);
	     			  remove_intent.putExtra("location", l_name);
	     			  startActivityForResult(remove_intent,1);
				}
			});
		}
		//The code in the function below checks to see if a new location was added and if so it refreshes the page
	    @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        super.onActivityResult(requestCode, resultCode, data);
	        
	        if(resultCode==5){
		        Intent in = new Intent();
		        setResult(1,in);//Here I am Setting the Requestcode 1, you can put according to your requirement
		        finish();
	        }
	        if(resultCode==1){
	            Intent refresh = new Intent(ViewLocation.this, ViewLocation.class);
	            refresh.putExtra("location", l_name);
	            startActivity(refresh);
	            this.finish();
	        }
	        if(resultCode==RESULT_CANCELED){
	        	EditLocation.setLocationVars(0.0,0.0,null,500,false); 
	        }
	        
		}
// Menu Layout
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			super.onCreateOptionsMenu(menu);
			menu.add(0,1,0,"Home").setIcon(R.drawable.home);
	        menu.add(0,2,0,"Edit").setIcon(R.drawable.edit_icon);
	        menu.add(0,3,0,"Delete").setIcon(R.drawable.delete_icon);
	        menu.add(0,4,0,"Cancel").setIcon(R.drawable.cancel);
		    return true;
		}
	    @Override
	     public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getItemId()) {
	        case 1:
				  Intent home_intent_menu = new Intent(ViewLocation.this, MainActivity.class);
				  startActivity(home_intent_menu);
   			  
   			  return true;
	        case 2:
   			  Intent edit_intent_menu = new Intent(ViewLocation.this, EditLocation.class);
   			  edit_intent_menu.putExtra("location", l_name);
   			startActivityForResult(edit_intent_menu,1);
			  this.finish();
   			  
   			  return true;
	        case 3:
     			  Intent remove_intent_menu = new Intent(ViewLocation.this, RemoveLocation.class);
     			  remove_intent_menu.putExtra("location", l_name);
     			 startActivityForResult(remove_intent_menu,1);
     			  this.finish();

	            return true;
	     case 4:
	    	 SmartResponseActivity.setCurrentTab(0);
			  Intent SR_intent_menu = new Intent(ViewLocation.this, SmartResponseActivity.class);
			  startActivity(SR_intent_menu);
			  finish();
	            return true;
	          }
			return false;
	    }
}